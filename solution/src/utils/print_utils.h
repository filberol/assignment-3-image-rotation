#ifndef PRINT_UTILS
#define PRINT_UTILS

void print(const char* const str);
void println(const char* const str);
void print_err(const char* const str);
void println_err(const char* const str);

#endif
