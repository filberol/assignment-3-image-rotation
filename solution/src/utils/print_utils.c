#include <stdio.h>

void print(const char* const str) { printf("%s", str); }
void println(const char* const str) { printf("%s\n", str); }
void print_err(const char* const str) { fprintf(stderr, "%s", str); }
void println_err(const char* const str) { fprintf(stderr, "%s\n", str); }
