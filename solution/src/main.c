#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "image/image_utils.h"
#include "utils/print_utils.h"

#include "bmp/bmp_manager.h"
#include "bmp/bmp_struct.h"
#include "files/file_manager.h"

#include "functions/rotate.h"

#include "stats/io_stats.h"
#include "stats/sys_msg.h"

#define DEF_OUT "out.bmp"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    //Check args
    switch (argc) {
    case 1: println(system_messages[LESS_ARGS]); exit(1); break;
    case 2: println(system_messages[NO_OUT]); break;
    case 3: break;
    default: println(system_messages[MORE_ARGS]); exit(1); break;
    }
    //Set up vaariables
    print(system_messages[WELCOME]);
    const char* source_filename = argv[1];
    const char* dest_filename;
    if (argc == 2) {
        dest_filename = DEF_OUT;
    } else {
        dest_filename = argv[2];
    }
    //Open source file
    FILE* source_file = NULL;
    enum open_stat src_open_res = open_file(&source_file, source_filename, "r");
    if (src_open_res != OPEN_NICE) { exit(1); }
    //Read source file
    struct image tmp_img = {0};
    enum read_stat read_res = read_bmp(source_file, &tmp_img);
    close_file(source_file);  
    if (read_res != READ_OK) { exit(1); }
    //Transfer
    struct image new_img = rotate(tmp_img);
    free_image(tmp_img);
    //Read destination file
    FILE* destination_file = NULL;
    enum open_stat dst_open_res = open_file(&destination_file, dest_filename, "w");
    if (dst_open_res != OPEN_NICE) { exit(1); }
    //Write file
    enum write_stat write_res = write_bmp(destination_file, new_img);
    close_file(destination_file);
    free_image(new_img);
    if (write_res != WRITE_OK) { exit(1); }
    println(system_messages[FINISHED]);
    exit(0);
    return 0;
}
