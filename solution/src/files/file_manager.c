#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "../stats/io_stats.h"
#include "../utils/print_utils.h"

enum open_stat open_file(FILE** out_file, const char* const filename, const char* const mode) {
    errno = 0;
    *out_file = fopen(filename, mode);
    if (*out_file == NULL) {
        print_err(open_file_messages[OPEN_BAD]);
        println_err(strerror(errno));
        return OPEN_BAD;
    }
    return OPEN_NICE;
}

enum close_stat close_file(FILE* file) {
    errno = 0;
    fclose(file);
    if (errno != 0) {
        print_err(close_file_messages[CLOSE_BAD]);
        println_err(strerror(errno));
        return CLOSE_BAD;
    }
    return CLOSE_NICE;
}
