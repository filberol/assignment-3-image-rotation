#include <stdio.h>

#include "../stats/io_stats.h"

#ifndef FILE_MAN_H
#define FILE_MAN_H

enum open_stat open_file(FILE** const out_file, const char* const filename, const char* const mode);
enum close_stat close_file(FILE* const file);

#endif
