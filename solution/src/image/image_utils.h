#include <stddef.h>
#include <stdint.h>

#include "image_struct.h"

#ifndef IMAGE
#define IMAGE

struct image allocate_image(const size_t width, const size_t height);
void free_image(const struct image img);

struct pixel get_pixel(const struct image* const img, size_t row, size_t column);
void set_pixel(const struct image* const img, size_t row, size_t columnn, struct pixel new_pixel);

#endif
