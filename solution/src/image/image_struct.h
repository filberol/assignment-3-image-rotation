#include <stddef.h>
#include <stdint.h>

#ifndef IMAGE_STRUCT
#define IMAGE_STRUCT

struct __attribute__((packed)) pixel { 
    uint8_t b, g, r; 
};

struct image {
    size_t width, height;
    struct pixel* pixels;
};

#endif
