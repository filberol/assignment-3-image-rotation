#include <malloc.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


#include "image_struct.h"

struct image allocate_image(const size_t width, const size_t height) {
    size_t size = sizeof(struct pixel) * (width+2) * height;
    struct pixel* alloc = malloc(size);
    // memset(alloc, 0, size);
    return (struct image){.width = width, .height = height, .pixels = alloc};
}

void free_image(struct image img) {
    free(img.pixels);
}

struct pixel get_pixel(const struct image* const img, size_t row, size_t column) {
    return img->pixels[img->width * row + column];
}

void set_pixel(const struct image* const img, size_t row, size_t columnn, struct pixel new_pixel) {
    img->pixels[img->width * row + columnn] = new_pixel;
}
