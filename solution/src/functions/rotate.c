#include <stddef.h>

#include "../image/image_utils.h"

struct image rotate(const struct image orig) {
    struct image new_img = allocate_image(orig.height, orig.width);

    for (size_t row = 0; row < orig.height; row++) {
        for (size_t column = 0; column < orig.width; column++) {
            set_pixel(&new_img, column, new_img.width - row - 1, get_pixel(&orig, row, column));
        }
    }
    return new_img;
}
