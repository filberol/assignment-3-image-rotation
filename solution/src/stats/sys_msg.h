#ifndef SYS_MSG_INC
#define SYS_MSG_INC

enum messages {
    WELCOME,
    FATAL_ERROR,
    FINISHED,
    LESS_ARGS,
    MORE_ARGS,
    NO_OUT
};

extern const char* const system_messages[];

#endif
