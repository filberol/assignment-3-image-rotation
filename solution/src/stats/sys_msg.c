#include "sys_msg.h"

const char* const system_messages[] = {
    [WELCOME] = "Processing _bmp_ picture...",
    [FATAL_ERROR] = "Fatal error occured during execution!",
    [FINISHED] = "Done.",
    [LESS_ARGS] = "Not enough arguments!",
    [MORE_ARGS] = "Too many arguments!",
    [NO_OUT] = "No output file, using default."
};
