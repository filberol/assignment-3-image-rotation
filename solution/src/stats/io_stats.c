#include "io_stats.h"

const char* const open_file_messages[] = {
    [OPEN_NICE] = "Opened file successfully",
    [OPEN_BAD] = "Error opening file: "
};

const char* const close_file_messages[] = {
    [CLOSE_NICE] = "Saved file successfully",
    [CLOSE_BAD] = "Error saving file: "
};

const char* const read_messages[] = {
    [READ_OK] = "Converted file successfully",
    [READ_INVALID_BITS] = "Error converting file: Invalid bits",
    [READ_INVALID_HEADER] = "Error converting file: Invalid header"
};

const char* const write_messages[] = {
    [WRITE_OK] = "Wrote image successfully",
    [WRITE_INVALID_BITS] = "Error writing content",
    [WRITE_INVALID_HEADER] = "Error writing header"
};
