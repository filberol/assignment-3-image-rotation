#ifndef IO_STATS_H
#define IO_STATS_H

enum open_stat {
    OPEN_NICE,
    OPEN_BAD
};

enum close_stat {
    CLOSE_NICE,
    CLOSE_BAD
};

enum read_stat {
    READ_OK,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_stat {
    WRITE_OK,
    WRITE_INVALID_BITS,
    WRITE_INVALID_HEADER
};

extern const char* const open_file_messages[];
extern const char* const close_file_messages[];
extern const char* const read_messages[];
extern const char* const write_messages[];

#endif
