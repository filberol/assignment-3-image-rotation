#include <stdint.h>
#include <stdio.h>

#include "../stats/io_stats.h"

#ifndef BMP_READ
#define BMP_READ

enum read_stat read_bmp(FILE* const file, struct image* const img);
enum write_stat write_bmp(FILE* const file, const struct image img);

#endif
