#include <stdint.h>
#include <stdio.h>

#include "./bmp_struct.h"
#include "../image/image_utils.h"
#include "../stats/io_stats.h"
#include "../utils/print_utils.h"

//Header
#define format_type 0x4d42
#define offset sizeof(struct bmp_header)
#define header_size 40
#define planes_c 1
#define compression_level 0
#define hor_density 2835
#define ver_density 2835
#define color_depth 24 //bit
#define colors_c 0
#define important_colors 0

static const struct bmp_header def_head = {
    .bfType = format_type,
    .bfReserved = 0,    //reserved field
    .bOffBits = offset,
    .biSize = header_size,
    .biPlanes = planes_c,
    .biCompression = compression_level,
    .biXPelsPerMeter = hor_density,
    .biYPelsPerMeter = ver_density,
    .biBitCount = color_depth,
    .biClrUsed = colors_c,
    .biClrImportant = important_colors
};

//Utils
uint8_t calculate_padding(size_t width) {
    uint8_t tmp = width * sizeof(struct pixel) % 4;
    if (tmp != 0) {tmp = 4 - tmp;}
    return tmp;
}

//Read utils
static enum read_stat read_header(FILE* file, struct bmp_header* header) {
    if (fread(header, sizeof(struct bmp_header), 1, file) != 1) return READ_INVALID_HEADER;
    return READ_OK;
}

static enum read_stat read_content(FILE* file, struct image* destination) {
    size_t width = destination->width;
    uint8_t padding = calculate_padding(width);

    for (size_t rown = 0; rown < destination->height; rown++) {
        size_t coln = fread(destination->pixels + width * rown, sizeof(struct pixel), width, file);
        if (coln != width) return READ_INVALID_BITS;
        int skip = fseek(file, padding, SEEK_CUR);
        if (skip != 0) return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum read_stat read_bmp(FILE* file, struct image* img) {
    struct bmp_header hd = {0};

    enum read_stat hd_st = read_header(file, &hd);
    if (hd_st != READ_OK) {
        println_err(read_messages[hd_st]);
        return hd_st;
    }

    *img = allocate_image(hd.biWidth, hd.biHeight);

    enum read_stat ct_st = read_content(file, img);
    if (ct_st != READ_OK) {
        println_err(read_messages[ct_st]);
        return ct_st;
    }
    return ct_st;
}

//Write utils
static enum write_stat write_header(FILE* file, struct image img) {
    struct bmp_header head = def_head;
    head.biSizeImage = sizeof(struct pixel) * img.width * img.height;
    head.bfileSize = head.biSizeImage + sizeof(struct bmp_header);
    head.biWidth = img.width;
    head.biHeight = img.height;

    if (fwrite(&head, sizeof(struct bmp_header), 1, file) != 1) return WRITE_INVALID_HEADER;
    return WRITE_OK;
}

static enum write_stat write_content(FILE* file, struct image img) {
    size_t width = img.width;
    uint8_t padding = calculate_padding(width);
    uint64_t zero = 0;
    
    for (size_t rown = 0; rown < img.height; rown++) {
        size_t coln = fwrite(img.pixels + width * rown, sizeof(struct pixel), width, file);
        if (coln != width) return WRITE_INVALID_BITS;
        fwrite(&zero, 1, padding, file);
    }
    return WRITE_OK;
}

enum write_stat write_bmp(FILE* file, struct image img) {
    enum write_stat hd_st = write_header(file, img);
    if (hd_st != WRITE_OK) {
        println_err(write_messages[hd_st]);
        return hd_st;
    }
    enum write_stat ct_st = write_content(file, img);
    if (ct_st != WRITE_OK) {
        println_err(write_messages[ct_st]);
        return ct_st;
    }
    return WRITE_OK;
}
